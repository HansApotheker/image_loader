function loadImage(url, id, x, y)
{
    var data = $.ajax({
        url: url,
        data: {
            'id': id,
            'x': x,
            'y': y
        },
        type: 'GET',
        async: false
    }).responseText;

    return 'data:image/jpeg;base64,' + decryptImage(data);
}

function decryptImage(image)
{
    binary = base64ToArray(image);
    binary[0] = 0xFF;
    binary[1] = 0xD8;
    binary[2] = 0xFF;
    binary[3] = 0xE0;
    return arrayToBase64(binary);
}

function base64ToArray(str)
{
    return new Uint8Array(atob(str).split("").map(function(c) {
        return c.charCodeAt(0);
    }));
}

function arrayToBase64(arr)
{
    return btoa(String.fromCharCode.apply(null, arr));
}