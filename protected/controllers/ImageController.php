<?php

class ImageController extends Controller
{
    public function actionList()
    {
        $criteria = new CDbCriteria();
        $count = Image::model()->count($criteria);

        $pages = new CPagination($count);
        $pages->pageSize = 5;
        $pages->applyLimit($criteria);

        /** @var Image[] $models */
        $models = Image::model()->findAll($criteria);
        $dataProvider = new CArrayDataProvider($models);
        $this->render('list', array(
            'dataProvider' => $dataProvider,
            'pages' => $pages
        ));
    }

    public function actionAdd()
    {
        $model = new ImageAddForm();

        if (isset($_POST['ImageAddForm']))
        {
            $model->attributes = $_POST['ImageAddForm'];
            $model->image = CUploadedFile::getInstance($model, 'image');
            if($model->validate())
            {
                $fileName = md5(rand() . $model->image->name);
                $extensionName = empty($model->image->extensionName) ? 'tmp' : $model->image->extensionName;
                $imagePath = ServiceUtils::getImagePath() . $fileName . '.' . $extensionName;

                $model->image->saveAs($imagePath);

                ServiceImage::scale($imagePath);

                $size = ServiceImage::crop(ServiceUtils::getImagePath() . $fileName . '.jpg', $model->splitHorizontal, $model->splitVertical);

                $image = new Image();
                $image->name = $fileName;
                $image->partWidth = $size['width'];
                $image->partHeight = $size['height'];
                $image->splitHorizontal = $model->splitHorizontal;
                $image->splitVertical = $model->splitVertical;
                $image->save();
            }
        }

        $this->render('add', array('model' => $model));
    }

    public function actionShow()
    {
        $id = Yii::app()->request->getQuery('id');

        /** @var Image $model */
        $model = Image::model()->findByPk($id);
        if ( ! $model)
        {
            throw new CHttpException(404, 'File not found');
        }

        $this->render('show', array(
            'model' => $model
        ));
    }

    public function actionFile()
    {
        $id = Yii::app()->request->getQuery('id');

        /** @var Image $model */
        $model = Image::model()->findByPk($id);
        if ( ! $model)
        {
            throw new CHttpException(404, 'File not found');
        }

        $positionX = Yii::app()->request->getQuery('x');
        $positionY = Yii::app()->request->getQuery('y');

        $filename = ServiceUtils::getImagePath() . $model->name . '_' . $positionX . '_' . $positionY . '.jpg';
        if ( ! file_exists($filename ))
        {
            throw new CHttpException(404, 'File not found');
        }

        header("Content-Length: " . filesize($filename));
        echo base64_encode(file_get_contents($filename));
    }
}