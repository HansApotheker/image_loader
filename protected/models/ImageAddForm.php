<?php

class ImageAddForm extends CFormModel
{
    public $image;
    public $splitVertical;
    public $splitHorizontal;

    public function rules()
    {
        return array(
            array('image, splitVertical, splitHorizontal', 'required'),
            array('image', 'file', 'types' => 'jpg, gif, png'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'image' => 'Image to load',
            'splitHorizontal' => 'Parts to split horizontal',
            'splitVertical' => 'Parts to split vertical',
        );
    }
}