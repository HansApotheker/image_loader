<?php

class ServiceImage
{
    /**
     * @param string $filename
     */
    public static function scale($filename)
    {
        $image = new Imagick($filename);

        $path_parts = pathinfo($filename);
        $dirname = empty($path_parts['dirname']) ? '' : $path_parts['dirname'];
        $filename = empty($path_parts['filename']) ? '' : $path_parts['filename'];
        $image->setImageFormat('jpg');

        if ($image->getimagewidth() > 400)
        {
            $image->scaleImage(400, 0);
        }

        $image->writeimage($dirname . '/' . $filename . '.jpg');
    }

    /**
     * @param string $file
     * @param integer $horizontal
     * @param integer $vertical
     * @return array
     */
    public static function crop($file, $horizontal, $vertical)
    {
        $image = new Imagick($file);
        $width = $image->getimagewidth();
        $height = $image->getimageheight();

        $cropWidth = floor($width / $horizontal);
        $cropHeight = floor($height / $vertical);

        $path_parts = pathinfo($file);
        $dirname = empty($path_parts['dirname']) ? '' : $path_parts['dirname'];
        $filename = empty($path_parts['filename']) ? '' : $path_parts['filename'];
        $extension = empty($path_parts['extension']) ? '' : $path_parts['extension'];

        $n = 0;
        $y = 0;
        for ($i = 0; $i < $vertical; $i++)
        {
            $x = 0;
            for ($j = 0; $j < $horizontal; $j++)
            {
                $image = new Imagick($file);
                $image->cropimage($cropWidth, $cropHeight, $x, $y);
                $image->writeimage($dirname . '/' . $filename . '_' . $j . '_' . $i . '.' . $extension);
                self::_cryptFile($dirname . '/' . $filename . '_' . $j . '_' . $i . '.' . $extension);

                $x += $cropWidth;
                $n++;
            }
            $y += $cropHeight;
        }

        return array(
            'width' => $cropWidth,
            'height' => $cropHeight,
        );
    }

    /**
     * @param string $filename
     */
    protected static function _cryptFile($filename)
    {
        $file = file_get_contents($filename);
        $file[0] = 0;
        $file[1] = 0;
        $file[2] = 0;
        $file[3] = 0;

        file_put_contents($filename, $file);
    }
}