<?php

class ServiceUtils
{
    /**
     * @return string
     */
    public static function getImagePath()
    {
        $dir = Yii::app()->params['fileDirectory'];
        if (empty($dir))
        {
            $dir = 'data';
        }

        return Yii::app()->getBasePath() .  '/../' . $dir . '/';
    }
} 