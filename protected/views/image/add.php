<h1>Load file</h1>

<div class="form">
<?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'add-form',
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
?>

<div class="row">
    <?php echo $form->labelEx($model, 'image'); ?>
    <?php echo $form->fileField($model, 'image'); ?>
    <?php echo $form->error($model, 'image'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'splitHorizontal'); ?>
    <?php echo $form->dropDownList($model, 'splitHorizontal', array_combine(range(2, 4), range(2, 4)), array('options' => array('size' => 1))); ?>
    <?php echo $form->error($model, 'splitHorizontal'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'splitVertical'); ?>
    <?php echo $form->dropDownList($model, 'splitVertical', array_combine(range(2, 4), range(2, 4)), array('options' => array('size' => 1))); ?>
    <?php echo $form->error($model, 'splitVertical'); ?>
</div>

<div class="row">
    <?php echo CHtml::submitButton('Submit'); ?>
</div>

<?php $this->endWidget(); ?>

</div>