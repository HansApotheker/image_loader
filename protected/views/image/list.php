<h1>Files</h1>

<?php
    $this->widget('CLinkPager', array(
        'pages' => $pages,
    ));
?>


<?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider' => $dataProvider,
        'columns' => array(
            'id',
            array(
                'name' => 'name',
                'value' => 'CHtml::link($data->name, Yii::app()->createUrl("image/show", array("id"=>$data->id)))',
                'type' => 'raw'
            ),
        )
    ));
?>