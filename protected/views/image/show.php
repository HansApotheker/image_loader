<?php
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile($baseUrl.'/js/show.js');
?>
<?php for ($j = 0; $j < $model->splitVertical; $j++): ?>
    <?php for ($i = 0; $i < $model->splitHorizontal; $i++): ?>
        <canvas width="<?php echo $model->partWidth ?>" height="<?php echo $model->partHeight ?>" id="<?php echo $model->id ?>"
                x="<?php echo $i ?>" y="<?php echo $j ?>"></canvas>
    <?php endfor; ?>
    <br />
<?php endfor; ?>
<script>
    $(document).ready(function(){

        $("canvas").click(function() {

            var myCanvas = document.getElementById('myCanvas');
            var ctx = this.getContext('2d');

            var id = $(this).attr('id');
            var x = $(this).attr('x');
            var y = $(this).attr('y');
            var img = new Image;
            img.src = loadImage('<?php echo Yii::app()->createUrl("image/file") ?>', id, x, y);
            img.onload = function(){
                ctx.drawImage(img, 0, 0);
            };

        });
    });
</script>